from sqlalchemy import Column, Integer, Text, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

DBSession = scoped_session(sessionmaker())
Base = declarative_base()


# many of the fields in the DB are not included in the models
# as they're not required to compute competitive matrices
class App(Base):
    __tablename__ = 'app'
    id = Column(Integer, primary_key=True)
    name = Column(Text)


class Sdk(Base):
    __tablename__ = 'sdk'
    id = Column(Integer, primary_key=True)
    name = Column(Text)


class App_Sdk(Base):
    __tablename__ = 'app_sdk'
    app_id = Column(Integer, ForeignKey('app.id'), primary_key=True)
    sdk_id = Column(Integer, ForeignKey('sdk.id'), primary_key=True)
    installed = Column(Integer)
