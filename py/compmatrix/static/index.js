/* Switch functions */
/* eslint-disable no-bitwise, no-unused-vars, no-use-before-define */
const getState = (target) =>
  target.attributes.state.nodeValue ^ 0;
const toggleState = (target) => {
  target.setAttribute('state', getState(target) ^ 1);
  fetchData();
};
/* eslint-enable no-bitwise, no-unused-vars, no-use-before-define */

/* Find a sum of a data row, used for normalizing */
const sumRow = (dataRow) =>
  Object.values(dataRow).map((a) =>
    a.count)
    .reduce((a, c) =>
      a + c);


/* Templating functions */
const tableCell = (title, hue, value, text) =>
  `<td 
    title="${title}" 
    style="background-color: hsl(${hue},100%,${value}%);"
    >
    ${text}
    </td>`;

const tableHeader = (text) =>
  `<th>${text}</th>`;

const tableRow = (header, text) =>
  `<tr><th>${header}</th>${text}</tr>`;

const errorMessage = (description) =>
  `An error has occurred while constructing the table. ${description}`;


/* Creates HTML of a cell in the table body */
const generateCell = (dataCell, sum, normalized) => {
  const { count, examples } = dataCell;
  const ratio = count / sum;
  const percentage = Math.round(ratio * 100);
  const value = Math.round(100 - (ratio * 30));
  const title = examples.join(', ');
  const hue = normalized ? 200 : 0;
  const text = normalized ? `${percentage} %` : count;
  return tableCell(title, hue, value, text);
};


/* Creates HTML of the whole table */
const updateData = (response) => {
  /* Parse response */
  const table = document.querySelector('table');
  table.innerHTML = '';
  if (!response.data) {
    table.innerHTML = errorMessage(response.description);
    return;
  }
  const { data } = response;

  /* Get list of SDKs and if the table should be normalized */
  const sdks = Object.keys(data);
  const normalized = Boolean(getState(document.querySelector('#normalize')));

  /* Construct the header row */
  const headerCellHTML = sdks.map((a) =>
    tableHeader(`To ${a}`)).join('');
  const headerRowHTML = tableRow('', headerCellHTML);

  /* Construct the table body */
  const mainRowHTML = sdks.map((fromSdk) => {
    const dataRow = data[fromSdk];
    const sum = sumRow(dataRow);
    const mainCellHTML = sdks.map((toSdk) =>
      generateCell(dataRow[toSdk], sum, normalized)).join('');
    return tableRow(`From ${fromSdk}`, mainCellHTML);
  }).join('');
  table.innerHTML = headerRowHTML + mainRowHTML;
};


/* Constructs the data URL, fetches data */
const fetchData = () => {
  const allSdks = Array.from(document.querySelectorAll('.sdk'));
  const selectedSdks = allSdks.map((a) =>
    (getState(a) ? a.textContent : null))
    .filter(Boolean)
    .join(';');
  const url = `http://localhost:6543/get_data/${selectedSdks}`;
  fetch(url).then((res) =>
    res.json())
    .then(updateData);
};

(function main() {
  fetchData();
}());
