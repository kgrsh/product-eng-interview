from wsgiref.simple_server import make_server
from pyramid.config import Configurator
# from pyramid.response import Response
from pyramid.view import view_config
from sqlalchemy import create_engine
from .models import DBSession, App, Sdk, App_Sdk
from itertools import islice
# from wsgi_lineprof.middleware import LineProfilerMiddleware
# from wsgi_lineprof.filters import FilenameFilter

HOST = '0.0.0.0'
PORT = 6543

all_sdks = [
    "Admob", "Alipay", "AppLovin", "Braintree", "Chartboost",
    "Facebook Payments", "Flurry", "PayPal", "PaymentKit", "RevMob", "Stripe",
    "Umeng MobClick", "card.io", "iAD Framework"
]


def intersections(sdk_names):
    # create a mapping of SDK names to their ids
    sdk_ids = {
        k: v
        for [k, v] in DBSession.query(Sdk.name, Sdk.id).filter(
            Sdk.name.in_(sdk_names)).all()
    }
    for sdk in sdk_names:
        if sdk not in sdk_ids:
            return {
                'result': 'error',
                'description': f'{sdk} not found in database'
            }

    # initialize main dictionary that will be returned
    data = {name: {name2: {} for name2 in sdk_names} for name in sdk_names}

    # initialize auxillary data structures
    installed = {}
    uninstalled = {}
    example_app_ids = []

    # query DB for a list of apps that installed or uninstalled each SDK
    for sdk in sdk_names:
        installed[sdk] = set(i[0] for i in DBSession.query(
            App_Sdk.app_id).filter_by(sdk_id=sdk_ids[sdk], installed=1).all())
        uninstalled[sdk] = set(i[0] for i in DBSession.query(
            App_Sdk.app_id).filter_by(sdk_id=sdk_ids[sdk], installed=0).all())

    # calculate retention data for selected frameworks
    for sdk in sdk_names:
        data[sdk][sdk]['count'] = len(installed[sdk])
        data[sdk][sdk]['examples'] = list(islice(installed[sdk], 3))
        example_app_ids += list(islice(installed[sdk], 3))

    # calculate attrition data for selected frameworks
    for from_sdk in sdk_names:
        for to_sdk in sdk_names:
            if from_sdk != to_sdk:
                intersection = uninstalled[from_sdk].intersection(
                    installed[to_sdk])
                data[from_sdk][to_sdk]['count'] = len(intersection)
                example_app_ids += list(islice(intersection, 3))
                data[from_sdk][to_sdk]['examples'] = list(
                    islice(intersection, 3))

    # initialize data dictionary for non-selected frameworks
    data['Other'] = {}
    for sdk in sdk_names:
        data[sdk]['Other'] = {}
        data['Other'][sdk] = {}
        data['Other']['Other'] = {}

    # calculate attrition data for non-selected frameworks
    all_installed = set().union(*installed.values())
    all_uninstalled = set().union(*uninstalled.values())
    all_ids = all_installed.union(all_uninstalled)
    for sdk in sdk_names:
        to_other = uninstalled[sdk].difference(all_installed)
        data[sdk]['Other']['count'] = len(to_other)
        example_app_ids += list(islice(to_other, 3))
        data[sdk]['Other']['examples'] = list(islice(to_other, 3))
        from_other = installed[sdk].difference(all_uninstalled)
        data['Other'][sdk]['count'] = len(from_other)
        example_app_ids += list(islice(from_other, 3))
        data['Other'][sdk]['examples'] = list(islice(from_other, 3))

    # create a mapping of app ids to their names
    example_app_names = {
        k: v
        for [k, v] in DBSession.query(App.id, App.name).filter(
            App.id.in_(example_app_ids)).all()
    }

    # examples of apps not using any of the selected frameworks are not
    # returned and their count is calculated rather than queried for
    # due to performance concerns

    data['Other']['Other']['count'] = DBSession.query(
        App.id).count() - len(all_ids)
    data['Other']['Other']['examples'] = []

    # replace app ids with their names
    for from_sdk in data:
        for to_sdk in data[from_sdk]:
            data[from_sdk][to_sdk]['examples'] = [
                example_app_names[i]
                for i in data[from_sdk][to_sdk]['examples']
            ]

    # other_query = DBSession.query(App.id).filter(App.id.notin_(all_ids))
    # data['Other']['Other']['count'] = other_query.count()
    # other_example_ids = [i[0] for i in other_query.limit(3).all()]
    # data['Other']['Other']['examples'] = [
    #     i[0] for i in DBSession.query(App.name).filter(
    #         App.id.in_(other_example_ids)).all()
    # ]

    return {"result": "ok", "data": data}


# return information about selected SDKs
@view_config(route_name='get_filtered', renderer='json')
def get_filtered(request):
    sdks = request.matchdict['sdks'].split(';')
    return intersections(sdks)


# return information about all SDKs if none are selected
@view_config(route_name='get_all', renderer='json')
def get_all(request):
    return intersections(all_sdks)


if __name__ == '__main__':
    engine = create_engine("sqlite:///data.db")
    DBSession.configure(bind=engine)
    with Configurator() as config:
        config.add_route('get_filtered', '/get_data/{sdks}')
        config.add_route('get_all', '/get_data/')

        config.add_static_view(name='/prototype', path='static')
        config.add_static_view(name='/', path='static-v2')
        config.scan()
        # filters = [FilenameFilter("__main__.py")]
        # app = LineProfilerMiddleware(config.make_wsgi_app(), filters=filters)
        app = config.make_wsgi_app()
    server = make_server(HOST, PORT, app)
    server.serve_forever()
