This project renders competietive matrices in a browser.

To run it, execute the following in a terminal.
```
cd product-eng-interview
nix-shell .
python -m compmatrix
```
then open one of the two frontends in your browser.

Two frontends are provided: one written in React, available at http://localhost:6543/ , and one in vanilla Javascript at http://localhost:6543/prototype/ . 
Their functionality is similar. 
Source for the React frontend can be found in the `frontend-react` folder, source for the Javascript one in the `py/compmatrix/static` .

![](https://gitlab.com/kgrsh/product-eng-interview/-/raw/master/screenshot1.png)
![](https://gitlab.com/kgrsh/product-eng-interview/-/raw/master/screenshot2.png)
![](https://gitlab.com/kgrsh/product-eng-interview/-/raw/master/screenshot3.png)
![](https://gitlab.com/kgrsh/product-eng-interview/-/raw/master/screenshot4.png)
