import React from 'react';

function Switch(props) {
  const { toggleFunction, label } = props;
  return (
    <div>
      <input type="checkbox" onChange={() => toggleFunction(label)} id={label} name={label}/>
      <label htmlFor={label}>{label}</label>
    </div>
  );
}

export default Switch;
