import React from 'react';

function TableCell(props) {
  const {
    data, sum, normalized,
  } = props;
  const { count, examples } = data;
  const ratio = count / sum;
  const percentage = Math.round(ratio * 100);
  const value = Math.round(100 - (ratio * 30));
  const title = examples.join(', ');
  const hue = normalized ? 200 : 0;
  const text = normalized ? `${percentage} %` : count;

  return <td style={{ backgroundColor: `hsl(${hue},100%,${value}%)` }} title={title}>{text}</td>;
}

export default TableCell;
