import React from 'react';
import { sumRow } from './util';
import TableCell from './TableCell';

function TableRow(props) {
  const {
    dataRow, fromSdk, sdks, normalized,
  } = props;
  const sum = sumRow(dataRow);
  return <tr key={fromSdk}>
              <th key={fromSdk}>From {fromSdk}</th>
              {sdks.map((toSdk) =>
                <TableCell
                  data={dataRow[toSdk]}
                  keyToUse={toSdk}
                  sum={sum}
                  normalized={normalized}
                  key={toSdk}
                />)}
            </tr>;
}
export default TableRow;
