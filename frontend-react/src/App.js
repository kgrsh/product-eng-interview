import React, { useState, useEffect, useCallback } from 'react';
import { initialSdkState } from './util';
import Switch from './Switch';
import TableRow from './TableRow';


function App() {
  const [sdkState, setAllSdks] = useState(initialSdkState);
  const [data, setData] = useState({});
  const [normalized, setNormalized] = useState(false);

  const fetchData = useCallback(() => {
    const selectedSdksList = Object.entries(sdkState)
      .filter(([key, val]) => val)
      .map(([key, val]) => key);
    const selectedSdks = selectedSdksList.join(';');
    const url = `http://localhost:6543/get_data/${selectedSdks}`;
    fetch(url)
      .then((res) => res.json())
      .then((res) => setData(res.data));
  }, [sdkState]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const toggleSdk = (name) => {
    setAllSdks({ ...sdkState, [name]: sdkState[name] = !sdkState[name] });
  };

  const toggleNormalized = () => {
    setNormalized(!normalized);
  };

  const sdks = Object.keys(data);

  return (
    <div>
    <div id="sdk-toggles">
        {Object.keys(sdkState)
          .map((key) => (
          <Switch toggleFunction={toggleSdk} key={key} label={key} />
          ))}
          <Switch toggleFunction={toggleNormalized} key="Normalized" label="Normalized?" />
    </div>
      <table>
        <thead>
          <tr>
            <th></th>
            {sdks.map((toSdk) => (
              <th key={toSdk}>To {toSdk}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {sdks.map((fromSdk) =>
            <TableRow
              key={fromSdk}
              dataRow={data[fromSdk]}
              fromSdk={fromSdk}
              sdks={sdks}
              normalized={normalized} />)}
        </tbody>
      </table>
    </div>
  );
}

export default App;
