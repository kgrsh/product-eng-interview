const allSdks = [
  'Admob', 'Alipay', 'AppLovin', 'Braintree', 'Chartboost',
  'Facebook Payments', 'Flurry', 'PayPal', 'PaymentKit', 'RevMob', 'Stripe',
  'Umeng MobClick', 'card.io', 'iAD Framework',
];

const initialSdkState = Object.fromEntries(allSdks.map((a) => [a, false]));

const sumRow = (dataRow) =>
  Object.values(dataRow)
    .map((a) =>
      a.count)
    .reduce((a, c) =>
      a + c);

export { initialSdkState, sumRow };
